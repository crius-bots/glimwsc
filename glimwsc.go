package glimwsc

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

const (
	GlimeshAPIURL     = "glimesh.tv/api"
	GlimeshAPIVersion = "2.0.0"
	GlimeshSocketURL  = "wss://" + GlimeshAPIURL + "/socket/websocket"

	_AbsintheControl = "__absinthe__:control"
)

var replacer = strings.NewReplacer("\n", " ", "\t", "")

type AuthMode uint

const (
	AuthModeToken AuthMode = 1 << iota
	AuthModeClientID
)

// Make a new client.
// this code doesn't handle getting the token, sorry.
func New(token string, authMode AuthMode) *Session {
	return &Session{
		responseChans: make(map[string]chan json.RawMessage),
		refCounter:    0,
		subscriptions: make(map[string]func(*Session, json.RawMessage)),
		token:         token,
		writeChan:     make(chan interface{}),
		stop:          make(chan bool),
		authMode:      authMode,
	}
}

type Session struct {
	wsConn        *websocket.Conn
	responseChans map[string]chan json.RawMessage
	refCounter    int64
	subscriptions map[string]func(*Session, json.RawMessage)
	token         string
	writeChan     chan interface{}
	stop          chan bool
	authMode      AuthMode
}

type _GQLContainer struct {
	Query     string      `json:"query"`
	Variables interface{} `json:"variables"`
}

func (s *Session) DoQuery(query string, variables map[string]interface{}) json.RawMessage {
	payload := &_GQLContainer{
		Query:     replacer.Replace(query),
		Variables: variables,
	}

	atomic.AddInt64(&s.refCounter, 1)

	waitChan := make(chan json.RawMessage)
	s.responseChans[strconv.Itoa(int(s.refCounter))] = waitChan

	message := newPhoenixTuple(_AbsintheControl, "doc", payload).ToSlice(s.refCounter)

	s.writeChan <- message

	data := <-waitChan

	return data
}

func (s *Session) Open() error {
	dialer := &websocket.Dialer{}

	var auth string
	switch s.authMode {
	case AuthModeToken:
		auth = "token"
	case AuthModeClientID:
		auth = "client_id"
	}

	conn, _, err := dialer.Dial(fmt.Sprintf("%s?vsn=%s&%s=%s", GlimeshSocketURL, GlimeshAPIVersion, auth, s.token), nil)
	if err != nil {
		return err
	}
	s.wsConn = conn

	// we have to send the first message
	joinMsg := newPhoenixTuple(_AbsintheControl, "phx_join", nil).ToSlice(0)

	err = conn.WriteJSON(joinMsg)
	if err != nil {
		return err
	}

	var data []json.RawMessage

	// read the first resp
	err = conn.ReadJSON(&data)
	if err != nil {
		return err
	}

	readyRes := &phoenixTuple{}
	readyRes.FromSlice(data)

	// before heartbeats, create the writer - it goes from a channel to the websocket
	go s.writer()

	// now start up the heartbeats
	go s.doHeartbeats()

	// this is the reader
	go s.reader()

	return nil
}

func (s *Session) Close() {
	s.stop <- true
	close(s.writeChan)
}

// stuff for doing ops on the connection

func (s *Session) writer() {
	for {
		select {
		case data := <-s.writeChan:
			{
				err := s.wsConn.WriteJSON(data)
				if err != nil {
					log.Fatal(err)
				}
			}
		case <-s.stop:
			return
		}
	}

	s.wsConn.Close()
}

func (s *Session) reader() {
	for {
		select {
		case <-s.stop:
			return
		default:
		}

		_, msg, err := s.wsConn.ReadMessage()
		if err != nil {
			log.Fatal(err)
		}

		var data []json.RawMessage

		json.Unmarshal(msg, &data)

		message := &phoenixTuple{}
		message.FromSlice(data)

		if message.Ref == "" && message.Event == "subscription:data" {
			// it's a sub
			if h, ok := s.subscriptions[message.Topic]; ok {
				onSub := &OnSubscription{}
				err := json.Unmarshal(message.Payload.(json.RawMessage), onSub)
				if err != nil {
					log.Println(err)
					continue
				}

				h(s, onSub.Result.Data)
				continue
			}
		}

		// maybe it's a reply?
		if channel, ok := s.responseChans[message.Ref]; ok {
			channel <- message.Payload.(json.RawMessage)

			delete(s.responseChans, message.Ref)
			close(channel)
			continue
		}
	}
}

func (s *Session) doHeartbeats() {
	ticker := time.NewTicker(time.Second * 28)
	defer ticker.Stop()

	heartbeat := newPhoenixTuple("phoenix", "heartbeat", nil).ToSlice(0)

	for {
		select {
		case <-ticker.C:
			s.writeChan <- heartbeat
		case <-s.stop:
			return
		}
	}
}
